<!-- Please update value in the {}  -->

<h1 align="center">My Gallery</h1>

<div align="center">
   Solution for a challenge from  <a href="http://devchallenges.io" target="_blank">Devchallenges.io</a>.
</div>

<div align="center">
  <h3>
    <a href="https://{[your-demo-link.your-domain](https://mygallery-silk.vercel.app/)}">
      Demo
    </a>
    <span> | </span>
    <a href="https://{[your-url-to-the-solution](https://gitlab.com/SebasttianDaza/mygallery)}">
      Solution
    </a>
    <span> | </span>
    <a href="https://devchallenges.io/challenges/gcbWLxG6wdennelX7b8I">
      Challenge
    </a>
    <span> | </span>
    <a href="https://www.figma.com/file/HHzg6Ywq8jamFTB0J4iXKM/my-gallery-challenge?node-id=1%3A2">
      Design
    </a> 
  </h3>
</div>

<!-- TABLE OF CONTENTS -->

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Overview](#overview)
  - [Built With](#built-with)
- [Features](#features)
- [Acknowledgements](#acknowledgements)
- [Contact](#contact)

<!-- OVERVIEW -->

## Overview

![screenshot](https://firebasestorage.googleapis.com/v0/b/emprendeyourlifestyle.appspot.com/o/myGallery.png?alt=media&token=4bf2c7a3-022a-4e3a-b544-9ef131e9f674)

Introduce your projects by taking a screenshot or a gif. Try to tell visitors a story about your project by answering:

- Where can I see your demo?
  Here it's: [Demo](https://mygallery-silk.vercel.app/)
- What was your experience?
  I could to learn much about CSS Grid and to improve my skills.
- What have you learned/improved?
  I learned how to use CSS Grid and to use it in my projects.
- Your wisdom? :)
  CSS is great.

### Built With

<!-- This section should list any major frameworks that you built your project using. Here are a few examples.-->

- [HTML](https://developer.mozilla.org/)
- [CSS](https://developer.mozilla.org/)

## Features

<!-- List the features of your application or follow the template. Don't share the figma file here :) -->

This application/site was created as a submission to a [DevChallenges](https://devchallenges.io/challenges) challenge. The [challenge](https://devchallenges.io/challenges/gcbWLxG6wdennelX7b8I) was to build an application to complete the given user stories.


## Acknowledgements

<!-- This section should list any articles or add-ons/plugins that helps you to complete the project. This is optional but it will help you in the future. For exmpale -->

- [Steps to replicate a design with only HTML and CSS](https://devchallenges-blogs.web.app/how-to-replicate-design/)
- [Node.js](https://nodejs.org/)
- [Marked - a markdown parser](https://github.com/chjj/marked)

## Contact

- Website [emprendeyourlifestyle](https://{[your-web-site-link](http://emprendeyourlifestyle.com/)})
- GitHub [@sebasttiandaza](https://{github.com/sebasttiandaza})
- Twitter [@SebasttianDaza](https://{twitter.com/SebasttianDaza})

